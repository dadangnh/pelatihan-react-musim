import React from 'react';
import MusimView from './MusimView';
import Spinner from './Spinner';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            lat: null,
            lon: null,
            errorMessage: ''
        }
    }

    componentDidMount() {
        window.navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    lat: position.coords.latitude,
                    lon: position.coords.longitude
                })
            },
            (error) => {
                this.setState({
                    errorMessage: error.message,
                })
            }
        );
    }

    renderContent = () => {
        if (this.state.errorMessage && !this.state.lat && !this.state.lon) {
            return (
                <div>
                    <p>{this.state.errorMessage}</p>
                </div>
            );
        }

        if (!this.state.errorMessage && this.state.lat && this.state.lon) {
            return (
                <div>
                    <MusimView lat={this.state.lat}/>
                </div>
            );
        }

        return <Spinner message="Getting location,,," />
    }

    render() {
        return (
            <div>
                {this.renderContent()}
            </div>
        )
    }
}

export default App;
