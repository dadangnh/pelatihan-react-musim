import React from 'react';

const Spinner = (props) => {
    return (
        <div className="ui active dimmer">
            <dic className="ui big text loader">
                {props.message}
            </dic>
        </div>
    )
}

export default Spinner;
