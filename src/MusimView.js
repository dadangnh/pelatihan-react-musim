import './MusimView.css';
import React from 'react';

const stringMusim = {
    summer: {
        text: "Let's hit the beach",
        iconName: 'sun'
    },
    winter: {
        text: "Brrr, it's cold!",
        iconName: 'snowflake'
    }
}

const getMusim = (lat, bulan) => {
    if (bulan > 2 && bulan < 9) {
        return lat > 0 ? 'summer' : 'winter';
    } else {
        return lat > 0 ? 'winter' : 'summer';
    }
}

const MusimView = (props) => {
    const musim = getMusim(props.lat, new Date().getMonth());
    const {text, iconName} = stringMusim[musim];

    return (
        <div className={`session-display ${musim}`}>
            <i className={`icon-left massive ${iconName} icon`} />
            <h1>{text}</h1>
            <i className={`icon-right massive ${iconName} icon`} />
        </div>
    )
}

export default MusimView;
